---

#---- Page Properties ----#
layout: library
contentType: library
post-date: 2020-02-16

#---- Language and Localization ----#
content_id: library-minimap-manager
lang: en

#---- Library-only Stuff ----#
infoBoxContent:
    creator: Almouse
    gitRepo: https://gitlab.com/ZSmith/dota2-modding-libraries

tags: 
    - UI
#   - util
#   - reference

#dependencies:
#    - library-timers

#related_libraries:
#   - library-timers

#---- Localizable Content ----#
title: Minimap Manager

# ----------------------------#
# /images/library/library-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/library/library-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this library's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this library (in the corresponding language)
# ----------------------------#

---

The Minimap Manager Library adds additional functionality to the core Dota minimap. This includes the ability to define custom minimap icons using panorama image files (rather than Dota KV values), as well as highlighting regions. You can also resize which parts of the map are shown on the minimap (for example, for travelling between regions).