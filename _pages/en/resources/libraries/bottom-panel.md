---

#---- Page Properties ----#
layout: library
contentType: library
post-date: 2020-02-16

#---- Language and Localization ----#
content_id: library-bottom-panel
lang: en

#---- Library-only Stuff ----#
infoBoxContent:
    creator: Almouse
    gitRepo: https://gitlab.com/ZSmith/dota2-modding-libraries

tags: 
    - UI
#   - util
#   - reference

#dependencies:
#    - library-timers

#related_libraries:
#   - library-timers

#---- Localizable Content ----#
title: "Bottom Panel"

# ----------------------------#
# /images/library/library-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/library/library-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this library's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this library (in the corresponding language)
# ----------------------------#

---

This is a simple reference implementation of the core Dota bottom panel. Each of the components have been entirely rewritten, meaning that they can be customized for the specific needs of your mod. The library currently supports:

- Health and Mana Bars
- Portraits (single and multiunit)
- Experience/Level Marker
- Buffs and Debuffs
- Abilities

It does **not** include:

- Talent tray
- Inventory and Stash boxes (try using the [containers](../containers) library)

The library operates fully panorama-side.