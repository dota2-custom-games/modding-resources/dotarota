---

#---- Page Properties ----#
layout: library
contentType: library
post-date: 2019-08-11

#---- Language and Localization ----#
content_id: library-containers
lang: en

#---- Library-only Stuff ----#
infoBoxContent:
    creator: BMD
    gitRepo: https://github.com/DarkoniusXNG/barebones

tags: 
    - UI
#   - reference

#related_libraries:
#    - timers

#---- Localizable Content ----#
title: Containers

---

The containers library enables the creation of objects which
store items. This includes mimicking core Dota components -
such as the inventory or stash systems.

Examples:
- Adding new options to inventory context menus
- Abilities that are cast on inventory items
- Custom shop systems
- Containers for carrying specific items (e.g. portal scrolls)