---

#---- Page Properties ----#
layout: library
contentType: library
post-date: 2020-02-16

#---- Language and Localization ----#
content_id: library-progress-bars
lang: en

#---- Library-only Stuff ----#
infoBoxContent:
    creator: Almouse
    gitRepo: https://gitlab.com/ZSmith/dota2-modding-libraries

tags: 
    - UI
#   - util
#   - reference

#dependencies:
#    - library-timers

#related_libraries:
#   - library-timers

#---- Localizable Content ----#
title: Progress Bars

# ----------------------------#
# /images/library/library-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/library/library-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this library's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this library (in the corresponding language)
# ----------------------------#

---

The Progress Bars library uses panorama to allow the creation of dynamic bars that move with units. These bars track the stacks or remaining duration of a modifier, determined upon instantiation.

Panorama allows for new progress bar styles to be easily defined using CSS. A traditional call to create a progress bar looks like:

```lua
local config = {
        progressBarType = "stacks",
        reversedProgress = false,
        style = "EnrageStacks",
        maxStacks = 100
}

ProgressBars:AddProgressBar(
    unit
    , "modifier_super_mutant_rage_stacks"
    , config
)
```

