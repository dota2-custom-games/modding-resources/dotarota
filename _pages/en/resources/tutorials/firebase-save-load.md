---

#--- Page Properties ---#
layout: tutorial
contentType: tutorial
list-ranking: 50
# (Higher list rankings will go LOWER on the tutorial page)

#--- Language and Localization ---#
content_id: tutorial-firebase
lang: en

#--- Sections/Headings (Tutorials Only) ---#
sections:
    - "Setting Up Firebase"
    - "Authentication"

#--- Localizable Content ---#
title: Creating a Simple Save/Load System

# ----------------------------#
# The body of the tutorial goes below the ---

# For titles:
#    ## Section title
# And add the title and "slugified" version to sections above
# For galleries:

#   ***
#   <div class="lightgallery" markdown="1">
#   [![Tutorial Image]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png)
#   ]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png"/>)
#   </div>
#   ... (repeat images as necessary)
#   ***

# For infoboxes:

# > error "title"
# > Test boop

# Valid infobox "types" are 'note' 'info' 'warning' 'error'
# 'info' should be used for things that are relevant to the tutorial in question
# 'note' should be used for asides (e.g. not directly relevant)
# the other two should remain unused unless it's important

#   Then convert the link to a single line
# ----------------------------#

---

# Introduction

In this tutorial we will demonstrate how to set up a simple save/load system using HTTP requests on an external web server. For convenience, we will make use of [Google Firebase](https://firebase.google.com/), which allows us to host a free online relational database.

As an example, we will create a simple game where each hero's position is saved every few seconds. The next time a player opens the game, their starting hero will instead spawn at the location saved from the previous session.

> info "Alternative Hosting"
> This tutorial is designed for learning about the requirements for interacting with a remote server, and getting started quickly. For a more advanced project you may want to look at other server hosting solutions, and will probably have to implement additional features to handle the save/load process rather than it happening continually.

The example mod built from this tutorial can be found
[on GitLab](https://gitlab.com/dota2-example-mods/dota2-firebase-example).

# Setting Up Firebase

Before we can start working on the Dota mod side of things, we need to set up a Firebase project that will store our game data. For our example project we will save information about players using their steam ID. We proceed as follows:

1. From the Firebase home page, create a new project (you shouldn't need Google Analytics or any of the other options)
2. Navigate to Storage > Database > Relational Database and create a new one
3. Take note of the URL of the database (for our example, it's *https://dota2-firebase-tutorial.firebaseio.com/*)
4. Go to the *rules* tab, and set it to: 

```json
"rules": {
  ".read": true,
  ".write": true
}
```

This has set up the database to accept any requests to read or write data that it receives. Obviously, this is insecure, but it will make do while we set other things up.

---

---

# Sending and Receiving Data

We quickly set up basic ingame functionality. We will create a new lua file, `heropositionmanager.lua`, whose job is to send information about hero locations every few seconds to the server, and load that information at game start. The core of our file is:

```lua
if HeroPositionManager == nil then
    HeroPositionManager = {}

    HeroPositionManager.SERVER_LOCATION = "https://dota2-firebase-tutorial.firebaseio.com/users/"
end    

function HeroPositionManager:initialize()
    self.initialized = true
    self.trackedHeroes = {}

    self:StartHeroTracking()
end

function HeroPositionManager:StartHeroTracking()
    -- Start the timer
    Timers:CreateTimer(5, function() 
        for steamid, hero in pairs(self.trackedHeroes) do
            self:SaveHeroPosition(steamid, hero)
        end
        -- Time until we run this function again
        return 5
    end)
end

function HeroPositionManager:OnHeroInGame(hero)
    local heroname = hero:GetUnitName()
    local steamid = PlayerResource:GetSteamID(hero:GetPlayerID())

    self.trackedHeroes[steamid] = hero
    
    self:LoadHeroPosition(steamid, hero)
end

function HeroPositionManager:SaveHeroPosition(steamid, hero)
    ...
end

function HeroPositionManager:LoadHeroPosition(steamid, hero)
    ...
end


if not HeroPositionManager.initialized then
    HeroPositionManager:initialize()
end
```

In order for this file to be loaded we need a `require(heropositionmanager)` line in an existing lua file (e.g. *gamemode.lua*).
We rely on some code in *events.lua* to run `OnHeroInGame` every time a new hero unit spawns. If our game lets players control multiple hero units we would have to be careful with this!

We can see what the core of this code does. This manager will continually keep a single timer running in the background, which runs every 5 seconds. When a hero is first added to the game, a call to `LoadHeroPosition` is made, and that hero is added to the table `trackedHeroes`. Later, when the timer triggers, each hero in game will trigger a call to `SaveHeroPosition`.

> note "Initialization"
> Having an `initialize` function which is called at the end of the file is a useful trick for creating new functions in separate namespaces (`HeroPositionManager:` here). This approach makes code relatively robust against the `script_reload` console command, and lets us store some state shared between a specialised collection of functions.

The Dota API allows us to make HTTP requests to our Firestore database in order to write the two missing functions. We'll start with `SaveHeroPosition`:

```lua
function HeroPositionManager:SaveHeroPosition(steamid, hero)

    -- Get the hero's position and convert it to a string
    local position = hero:GetAbsOrigin()
    putData = {}
    putData.x_position = position.x
    putData.y_position = position.y
    putData.z_position = position.z

    local encoded = json.encode(putData)

    local request = CreateHTTPRequestScriptVM( 
        "PUT",
        HeroPositionManager.SERVER_LOCATION..tostring(steamid)..'.json'
    )
    
    request:SetHTTPRequestRawPostBody("application/json", encoded)

    request:Send( 
        function( result )
            -- Do nothing
        end
    )
end
```

There are a few points of note here:

- We use the Lua builtin *json* library, which allows us to convert a Lua table to a json string.
- The `CreateHTTPRequestScriptVM` function allows us to start building the message we will send to the server. Because we want to update an entry, or add one if it doesn't exist, we want to use a `PUT` request.
- The `Send` function will submit the request. It takes as an argument a callback function which will run when the response to the request comes back from the server.

Because we're only sending data, we don't **need** to do anything with the response from the server. However, the response may include useful information such as the error code if the send fails.

For our `LoadHeroPosition` function, we will need to do something with the response:

```lua
function HeroPositionManager:LoadHeroPosition(steamid, hero)

    local request = CreateHTTPRequestScriptVM( "GET", HeroPositionManager.SERVER_LOCATION..tostring(steamid)..'.json')

    request:Send( function( result )

        -- Could we contact the server?
        if not (result.StatusCode == 200) then
            print("HeroPositionManager | Load Failed - Unable to contact server")
            return false
        end

        local obj, pos, err = json.decode(result.Body, 1, nil)

        -- Were we able to retrieve data?
        if not obj then
            print("HeroPositionManager | Load Failed - Could not find any data")
            return false
        end

        -- We got some data, set it!
        hero:SetAbsOrigin(Vector(obj.x_position, obj.y_position, obj.z_position))

    end )

end
```

In this case, we send a `GET` request, because we want to retrieve data from the server. Several things could go wrong - we might be unable to contact the server, it might not accept our request, or there might not be any data for the hero (for example, if it is the player's first time playing).

Assuming the request does succeed, we can update the hero's position. The variable `obj` contains the main body of the reponse - we know to use the `x_position` field because that is the value we set in the `SaveHeroPosition` function.

We can load this mod in tools mode and reload it a few times to test all of our code is working. Further, we can view the database inside the Firestore page, to check it is receiving information as we move our hero around!

---

---

# Authentication

Earlier we set our database to accept all read and write requests. Let's fix this now so that it will only accept trusted requests.

The Dota API grants us access to the `GetDedicatedServerKeyV2( string )` function, which generates a unique secret key for our mod to use once it is deployed on Valve's servers. Unfortunately we can't use it while we're in tools mode, but we can set everything up so that it will work when we're ready to publish our mod.

> note "On Secrecy"
> Anyone who downloads your mod can view its Lua code. It is not safe to store any secret values inside your code. Make sure you hide any values you use for testing your server before you publish!

Firebase contains a relatively advanced system for authenticating users. Sadly we are limited to HTTP requests inside the Dota tools, so we will make use of a trick to ensure that we only accept requests that know our secret value.

In particular, given our secret key, we will save all of our game data in a table on our database whose name contains that key. All other read/write access will be blocked. Our new firebase rules will look like this:

```json
  "rules": {
    "dedicated-server-key": {
      ".read": true,
      ".write": true
    }
    ,"DB_Testing": {
      ".read": false,
      ".write": false
    }
  }
}
```

When our game is released on to the Dota workshop we will replace the value `dedicated-server-key` with the key that is chosen by the Valve servers. We keep a value `DB_Testing` to let us test things in tools mode, but we should be careful to have access disabled when we're not actively using it.

We can then simply update the top of *heropositionmanager.lua* as follows:

```lua
if HeroPositionManager == nil then
    HeroPositionManager = {}
    
    HeroPositionManager.AUTH_KEY = "DB_Testing"
    if IsDedicatedServer() then
        HeroPositionManager.AUTH_KEY = GetDedicatedServerKeyV2("firebase-example")
    end

    HeroPositionManager.SERVER_LOCATION = "https://dota2-firebase-tutorial.firebaseio.com/"
    ..tostring(HeroPositionManager.AUTH_KEY)
    .."/users/"
end    
```

We can replace `"firebase-example"` with any string we like - such as the version number, to change the secret key every time there is a significant game update.

We can now reload the game inside tools mode. We should see that there have been no changes to functionality, but this will make us more confident that our mod will be secure when it's ready to be published!