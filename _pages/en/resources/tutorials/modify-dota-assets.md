---

#--- Page Properties ---#
layout: tutorial
contentType: tutorial
list-ranking: 5
# (Higher list rankings will go LOWER on the tutorial page)

#--- Language and Localization ---#
content_id: tutorial-modify-dota-assets
lang: en

#--- Sections/Headings (Tutorials Only) ---#
sections:
    - "Units, Heroes and Abilities"
    - "Tooltips"
    - "Models, Textures and Particles"
    - "UI Components"

#--- Localizable Content ---#
title: Modifying or Replacing Core Dota Components

# ----------------------------#
# The body of the tutorial goes below the ---

# For titles:
#    ## Section title
# And add the title and "slugified" version to sections above
# For galleries:

#   ***
#   <div class="lightgallery" markdown="1">
#   [![Tutorial Image]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png)
#   ]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png"/>)
#   </div>
#   ... (repeat images as necessary)
#   ***

#   Then convert the link to a single line
# ----------------------------#

---

# Introduction 

In this tutorial, we will look at how we can use and modify core Dota
components in arcade games. In particular, these methods can allow us
to more conveniently use game components without having to fully remake
them ourselves, or change some parts of how the game looks and feels.

Depending on the thing we are changing, a slightly different approach
must be made in each case. In each case, we'll give simple example
where we perform such a change.

> info "Internal names"
> In several cases we will need to know the internal name for a Dota component in order to be able to change it. A good way to do this is to look it up in the Game Tracking repository, e.g. by looking at the [Dota Tooltips](https://raw.githubusercontent.com/SteamDatabase/GameTracking-Dota2/master/game/dota/resource/dota_english.txt)

# Units, Heroes, Items and Abilities

Dota allows us to easily change information about the units, heroes, items
and abilities in our game. This is done by creating *Dota KV* entries corresponding
to these elements in the appropriate location, inside the `npc/scripts/` folder.
When Dota detects entries with the same name as existing Dota components, it will
instead load the custom versions instead of the built-in ones.

We can proceed as follows:

1. Find the internal Dota name for the asset we wish to replace.
2. Create a new `kv` file with a key to the corresponding unit, and add a
reference to this kv file (e.g. in `npc_custom_abilities.txt`).
3. Set the values in this kv file to be as similar or different to the unit
we are changing we desire.

> note "Lua-based Approach"
> For more complex solutions, or for batch changes such as changing how **all**
> heroes work, we might instead solve the problem using Lua. By listening to
> events like `OnNPCInGame`, we can modify the attributes of a unit or hero the
> moment they spawn.

## Units, Heroes, Items and Abilities - Example

In this example, we will change the attributes of lane creeps.

# Tooltips

Just as in the previous case, adding custom values in an appropriate place
will allow us to modify Dota tooltips. In particular, this lets us change
the display name of units or abilities, or even things like the tips that
appear when we pause the game, or chat wheel options.

Follow these steps:

1. Find the tooltip which we wish to replace
2. Add a new entry to `addon_english.txt` (or whichever localisation you are using)
with the same key as this tooltip.
3. Set this new tooltip to whatever we need

Note that changing tooltips often requires reloading the Dota 2 tools before
the changes will take effect in your custom game.

## Tooltips - Example

In this example, we will change the hint tooltips when players pause the game.

# Models, Textures and Particles

Dota also supports overrides for Models, Textures and Particles. Unlike the
previous cases, these require a little more work to get done.

The basic steps we must take in order to replace one of these things is as
follows:

1. Identify the path to the asset we wish to replace
2. Create a new file in the same relative location in our mod directory, with
roughly the same structure
3. If needed, add any necessary related files, and change the dependencies of our
replacement version

## Models, Textures and Particles - Example

For this example, we will change the minimap icons of heroes by replacing the
texture that stores these things.

# UI Components

Unlike the other assets so far, we cannot change the core Dota UI by replacing
files. Adding some `css` files in the same relative paths as their core Dota 
equivalent may make visible changes in tools mode, **but these changes will not
work on versions published to the Steam Workshop**.

However, we can look up the `xml` and `css` files that Dota uses in order to
see the general structure, or use the Panorama Debugger (F6) to select elements
of interest. We can then use Javascript HUD traversal to change how these
elements appear to players. The general approach is:

1. Identify HUD elements we want to change.
2. Create a javascript file for modifying these HUD elements, and include it
inside our `custom_ui_manifest.xml`.
3. Retrieve these HUD elements using Javascript HUD Traversal.
4. Apply any custom styles to the selected HUD elements.

## UI Components - Example

For this example, we will hide the talent tray for heroes. We might want to 
do this if we are playing a custom game mode where no units have talents.