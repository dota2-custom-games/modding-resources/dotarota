---

#--- Page Properties ---#
layout: tutorial
contentType: tutorial
list-ranking: 2
# (Higher list rankings will go LOWER on the tutorial page)

#--- Language and Localization ---#
content_id: tutorial-import-library
lang: en

#--- Sections/Headings (Tutorials Only) ---#
sections:
    - "Setting up Libraries"
    - "Example - Treasure Hunt Game"

#--- Localizable Content ---#
title: Importing and Using Libraries

# ----------------------------#
# The body of the tutorial goes below the ---

# For titles:
#    ## Section title
# And add the title and "slugified" version to sections above
# For galleries:

#   ***
#   <div class="lightgallery" markdown="1">
#   [![Tutorial Image]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png)
#   ]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png"/>)
#   </div>
#   ... (repeat images as necessary)
#   ***

#   Then convert the link to a single line
# ----------------------------#

---

# Introduction

In this tutorial we will see how to import and use Dota 2 modding libraries
made by other people. Libraries are a great resource for simplifying the
mod creation process, by providing pre-made functions that enable complex
functionality.

Because of the complexity of Dota custom games, it is often not as simple as
copy-pasting the files for a library into your custom game. This is why
templates such as barebones include a selection of the most useful libraries for
new modders. We'll show how to import libraries from scratch, setting up the
necessary hooks in our existing code.

For demonstration, we will import the *Progress Bars* library by Almouse, which
uses Panorama to allow showing the duration or stack count of specific modifiers.
We'll then make a simple game involving finding a hidden treasure.

# Setting up Libraries

The majority of libraries are stored as Git repositories. You can find a
collection of them on this site. We proceed as follows:

1. Download the library from the Git repository.
2. Move the downloaded files into the appropriate folders, based on their
file type.
3. Add `includes` of various types where needed to the added files
4. Configure the library and/or change any file paths used inside it

Generally speaking, most libraries will consist of one or two *.lua* or *.xml*
files, which represent the "main" component of the library, and may include
several additional files (such as *.css*, *.js*, *.vmat*...). When we add
a file, we usually have to include a reference somewhere in our code
to tell Dota that we have added a new file and we want to load it. Some of
these references may already be included in the library files, but you might
need to configure them if the folder structure of your custom game is different
to that of the library.

Based on the filetype, you will need to copy library files and add include
statements as follows:

- *.lua* files go in the *game/scripts/vscripts* directory. They are added to your codebase with a `require('path_to_file.lua')` statement in an existing lua file (such as `gamemode.lua`)
- *.xml* files go in the *panorama/layout/custom_game/* folder. Usually they're included by adding to `custom_ui_manifest.xml`:

```xml
<CustomUIElement 
    type="Hud"
    layoutfile="file://{resources}/layout/custom_game/path_to_file.xml"
/>
```

- If the library adds any texture, material or particle files, it is a good idea to precache them in the `addon_game_mode.lua` file's `Precache()` function, for example:

```lua
PrecacheResource(
    "particle_folder",
    "particles/path_to_content",
    context
)
```

Finally, most libraries contain additional files which are included inside the core *xml* or *lua* files. You should check that the paths to these files is correct after you have copied them across.

---

# Example - Treasure Hunt Game

As an example, we will use the *Progress Bars* library to make a simple
treasure-hunting game for one player.

At the start of the game, a random location on the ground will be chosen
as the location of the treasure. A bar will appear above the player hero's
head indicating how close they are to the treasure. Reaching the location of
the treasure will cause the Radiant team to win the game.

We use a simple barebones mod as a starting point - for example, one produced
by the [Mod Setup](../setup-simple-mod) tutorial.