---

# Page Properties
layout: tutorial
contentType: tutorial
list-ranking: 1

# Language and Localization
content_id: tutorial-simple-mod
lang: en

# Sections/Headings (Tutorials Only)
sections:
    - "Initializing a Mod"
    - "Mod Folder Structure"
    - "Setting up Basic Functionality"

# Localizable Content
title: Setting up a mod from Barebones
---

In this tutorial we will:

- Download a custom game template and set it up for Dota
- Explore the basic folder structure of a Dota mod
- Set up basic functionality and game end conditions using Lua

By the end, we will have an extremely simple custom game, played on a customizable map.

## Initializing a Mod

Although we can set up an empty mod from inside Dota, it can be a little inconvenient to get started. A barebones template can help configure the basic settings of our mod, which will make life a little easier.

There are a handful of barebones template available. We will use [DarkoniusXNG's branch of barebones](https://github.com/DarkoniusXNG/barebones). To set up:

1. Clone or download the barebones repository to a convenient location.
2. Navigate to your Steam Dota folder. Most likely, it will be located somewhere like `.../steamapps/common/dota 2 beta/`.
3. We will copy two folders from the barebones repository. Supposing we want to call our mod *tutorial_mod*:
   - Create a new folder at `.../dota 2 beta/content/tutorial_mod`. Copy the contents of the `content/dota_addons/barebones` folder into this folder.
   - Create a new folder at `.../dota 2 beta/game/tutorial_mod`. Copy the contents of the game/dota_addons/barebones folder into this folder.
4. Open Dota 2 in tools mode. You might need to install it first, by checking the "Dota 2 Workshop Tools DLC" item in the Steam page for Dota.
5. In the tools launch menu, you should now be able to select *tutorial_addon* as the addon to launch with.

> info "Developer Console"
> It will be very useful for us to enable the developer console, by adding the `-vconsole` launch flag to Dota.
> This allows us to view error messages if our scripts fail to run.

We've now successfully set up barebones!

***
<div class="lightgallery">
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/barebonesclone.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/barebonesclone.png" />
    </a>
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/folderstructure.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/folderstructure.png" />
    </a>
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/dotaworkshoptools.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/dotaworkshoptools.png" />
    </a>
</div>
***

## Mod Folder Structure

Before we get started working on our mod, let's have a quick look at some of the most important files and folders. Recall that a Dota mod is split into a *content* folder and a *game* folder. For convenience, we will refer to folders of the form `.../dota 2 beta/content/tutorial_mod/X` as `content/X` from now on, and similarly for `game/X`.

The `content` folder contains uncompiled components for our mod. If we want to add new models, textures or sounds, they will go in the content folder. 

​
`content/maps` contains the map files that will be used in our Dota mod. A Dota custom game can have multiple different maps. Only one map can be loaded in each game lobby, though.

`content/panorama` contains the materials used for any custom user interface components we will need. Dota user interface design should be familiar to anyone who has worked with HTML. Files are split into the folders `layout`, `styles`, and `scripts`, corresponding to XML, CSS and JavaScript components.

`content/panorama/layout/custom_game/custom_ui_manifest.xml` is a special file which forms the "root" of the UI. If we ever want to add new UI components, we will have to first include them from here.

The `game` folder contains the compiled versions of any files stored in the `content` folder. Most of the time these files will be automatically compiled on-demand. It is also where we will add any code or data files - information about units and abilities, map configurations, and so on.


`game/addoninfo.txt` contains the basic structural information about the mod. Here we choose how many players can join a lobby, how many teams there are, and so on.

`game/resource/addon_english.txt` contains the English language localizations for our custom game. If we ever add a new unit, ability, or UI component to our game, we will add corresponding English language strings here. If wanted, we can also add new files in this folder for other languages.

`game/scripts` contains the data files and code for our game. It is likely where we will spend most of our time making it unique. `game/scripts/npc` contains data files in the Dota KV (keyvalue) format. These let you overwrite the functionality of existing Dota components, such as the effects of items or abilities, or which heroes are available. `game/scripts/vscripts` is used to add Lua files, which change the game's logic.

---

## Setting up Basic Functionality

We will examine a little more closely some of the Lua code that makes our mod run. We're going to make the following (thrilling) game:

- Everyone plays as Tiny, and is on the Radiant team
- If a player walks to the right hand side of the map, the Dire team wins
- If a player walks to the left hand side of the map, the Radiant team wins

The first thing we need to do is test that our game can run at all. Since this is our first time opening this mod, our map file only exists in the `content` folder. This means it hasn't been compiled yet - and we won't be able to play our mod until it has.

To do this, we will open the map `template_map` in the Hammer map editor. From the Asset browser we can see several development tools - the icons in the top row. The Hammer icon is the first. Open it, and then open the template map inside it. To build the map, we press F9, and then select "Build" at the bottom.

After the build process has complete, we should hopefully load into our map! Right now, it is not so interesting: after selecting our hero, we are left in an empty square with nothing to do. Let's set up some basic configuration for the map using Lua code.

Barebones contains some framework code to allow us to make simple modifications more readily. The first file we will look at is `game/scripts/vscripts/settings.lua`. This file contains a collection of constant values for making simple changes during game setup. We're going to change the following lines:

```lua
CAMERA_DISTANCE_OVERRIDE = 1800
POST_GAME_TIME = 5.0
FORCE_PICKED_HERO = "npc_dota_hero_tiny"  
MAX_NUMBER_OF_TEAMS = 1
```

After saving this, we can re-launch the mod. Because we have made only code changes, we can "run" the map instead of "building" it, either by using the "Run" button in Hammer, or by typing in the vconsole:

```
dota_launch_custom_game tutorial_addon template_map
```

> note "Hot-reloading scripts"
> We don't even have to restart the map to test small code changes - typing `script_reload` in the developer console will reload the Lua files. Most panorama changes will also be immediately updated when we modify files, too.

After reloading the map, we should see that some changes have been made:

- All players are now on the Radiant team
- The hero selection phase and pregame phases were skipped, and instead we spawned as Tiny
- The camera is somewhat zoomed out

<div class="lightgallery">
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/hammerbuild.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/hammerbuild.png" />
    </a>
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/barebonesloading.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/barebonesloading.png" />
    </a>
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/ingame.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/ingame.png" />
    </a>
</div>

Finally, we'll add some simple "game end" conditions. If any unit moves too far to the East, the Dire team will win, whilst a unit moving too far to the West will make the Radiant team win. To do this, we'll make use of the `events.lua` file. The Dota API allows us to register event handlers for lots of important game events. Barebones includes some helpful functions to let us customise what happens in these situations.

> note "Filters"
> We can also use `filters.lua` to run code in reaction to certain ingame actions. Generally, **events** run *immediately after* something happens (a unit is created or dies, an ability is cast), while **filters** run *just before* something happens (a unit is about to take damage, a player issues an order to a unit but before the unit reacts to the order). 

We're going to add an event that runs every time a unit spawns. It will then check the position of that unit every second, and if their X coordinate satisfies a certain condition, we'll end the game. To do this, we add the following code to the function `OnNPCSpawned` in `events.lua`.

```lua
Timers:CreateTimer(1, function() 
    local x_position = npc:GetAbsOrigin().x
    print("Unit has position: "..tostring(x_position))
    if x_position > 1000 then
        GameRules:SetGameWinner(DOTA_TEAM_GOODGUYS)
        return false
    elseif x_position < -1000 then
        GameRules:SetGameWinner(DOTA_TEAM_BADGUYS)
        return false
    end
    return 1
end)
```

We won't spend too much time explaining the Lua code (there are tutorials elsewhere for basic Lua), but note

-  `Timers:CreateTimer` is a part of the `Timers` Library. It will call the function after 1 second (the first argument), and then again every second after that (the return value of the function).
- `GetAbsOrigin()` and `GameRules:SetGameWinner()` are functions from the [Dota Lua API](https://developer.valvesoftware.com/wiki/Dota_2_Workshop_Tools/Scripting/API). `DOTA_TEAM_GOODGUYS` is a constant value - we could have also written `GameRules:SetGameWinner(2)`, but this way is more readable.
- The variable `npc` shares the same scope as the `OnNPCSpawned` function. This code block could lead to bugs if the unit referenced by `npc` dies!


With this code in place, we're free to test our minimalistic game mode!

***
<div class="lightgallery">
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/modifiedOnNpcSpawned.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/modifiedOnNpcSpawned.png" />
    </a>
    <a href="../../../../images/tutorial/tutorial-setup-simple-mod/gameEnd.png">
        <img src="../../../../images/tutorial/tutorial-setup-simple-mod/gameEnd.png" />
    </a>
</div>