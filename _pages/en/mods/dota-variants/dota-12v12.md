---

#---- Page Properties ----#
layout: mod
contentType: dota-variant
post-date: 2019-08-15

#---- Language and Localization ----#
content_id: mod-dota-12v12
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
   steamWorkshop: 1576297063
   creator: Dota 2 Unofficial
   playerCount: 24
   duration: 30-40
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
#   qq:
#   gitRepo:

tags: 
    - teams
#   - freeForAll
#   - cooperative
#   - survival
#   - towerDefense

supported_languages:
    - en
    - sc
    - ru
#    - tc

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Dota 12v12"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

It's a 12 vs 12 battle royale as two teams compete to destroy the other's Ancient.

Featured in The International 2015 All-Star Match, Dota 12v12 uses the same basic rules as Dota, with increased gold and experience gains. Some heroes are not available in Dota 12v12 for balance.