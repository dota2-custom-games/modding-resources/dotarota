---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-roshpit-champions
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 469311655
    creator: ChalkyBrush
    discord: https://discord.gg/ZUhfMZG
    website: www.roshpit.ca
    playerCount: 1-4
    duration: 30-90
#   qq:
    gitRepo: https://github.com/ChalkyBrush/roshpit-bug-tracker

tags: 
    - cooperative
#   - survival
#   - teams
#   - towerDefense
#   - freeForAll
#   - beta
    - rpg

supported_languages:
     - en
     - sc
#    - tc
     - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Roshpit Champions"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Multiplayer Action RPG with permanent character saving and complex progression.

Note for new players - start your new game on Normal difficulty!
- 23 Unique Heroes with Custom Skill Trees
- Save and Load your Heroes to Complete 3 Difficulties
- 270 Legendary Items with Custom Affixes (+ 84 Legendary Weapons - 3 Unique Legendary Weapons for each hero)
- 30 Ultra Legendary items that change your heroes abilities
- Gear Equip System with 6 Slots
- Custom Enemies with Challenging AI and abilities
- 5 Max-size maps full of scripted events

## Other Credits
- Rachop - World 1 Map & Terrain
- songcx - Chinese Translation
- Disabre - Russian Translation
- Random 5th - Mobile App
- 1nsig(nia) - Graphics & QA
- invalid_nick - Awesome house models on Redfall Ridge
- Benedict Kataglou - Many Quality Icons