---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-spin-td
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 497032202
    creator: Aertz
    discord: https://discord.gg/Z5BhzfX
    playerCount: 1-4
    duration: 30-60
#   website: http://website.com
#   qq:
#   gitRepo:

tags: 
    - cooperative
    - towerDefense
#   - survival
#   - teams
#   - freeForAll
#   - beta
#   - rpg

supported_languages:
     - en
#    - sc
#    - tc
#    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-gem-td
    - mod-element-td

#---- Localizable Content ----#
title: "Spin TD"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

A 4 player Tower defense map, where you build towers by selecting preplaced markers on the map to build your towers.
You can also spin the slot machine in the center of the map by using your hero ability.
There are currently 50 waves and 9 bosses.
