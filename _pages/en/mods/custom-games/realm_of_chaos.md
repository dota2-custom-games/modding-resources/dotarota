---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-realm-of-chaos
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 520323982
    creator: Avalon Studio 
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
    playerCount: 1-10
    duration: 30-60
#   qq:
#   gitRepo:

tags: 
    - cooperative
#   - survival
#   - teams
#   - towerDefense
#   - freeForAll
#   - beta
    - rpg

supported_languages:
     - en
     - sc
#    - tc
#    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Realm of Chaos"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---
Welcome to Realm of Chaos

There are five heroes at the start for you to choose from. Players must complete quests and at the same time handle waves and waves of attacks from the enemy army swarming at your base.

You must work together diligently in order to beat the game! Your ultimate goal in this game is to become a Saint by finishing the main storyline. (Every drop could be useful or totally useless under different circumstances, please wisely use your drops, points, souls, money and most importantly, your TIME, have a nice racing experience)

Production Team:
- Producer: 牛蛙君
- Executive Producer: Kusomania
- Game Producer: 风见幽香(Archer)
- Game Designer: 风见幽香(Archer) & 泪星云
- Main Programmer: 风见幽香(Archer)
- Programmer: RobinCode & 平衡君
- Dialogue: Roger, 人形, 何以解忧
- Map Design: Mreak & Lily
- Model: 夜明黔
- Interface: F1rstDan & RobinCode
- Art Design: 红莲太刀 & F1rstDan
- Sound Efficts: 夜天X绝对领域
- Character Voice: 人形(Cloud), 寒昭(Foxy), 求虐(Rondu), 恋小哀(Morrow), 优塔(Phoebe)
- Special Efficts: 风见幽香(Archer)
- Translation: 138bpm, PZH_Element, Kusomania, Roger, GZSY
- Assistant: 我是小栋
- Visual Art: 骨灰