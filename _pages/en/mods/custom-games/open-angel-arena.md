---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2019-08-15

#---- Language and Localization ----#
content_id: mod-open-angel-arena
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
   steamWorkshop: 881541807
   creator: Thinking Bottle
   discord: https://discord.gg/9ApbMyW
   website: http://oaa.gg
   playerCount: 10
   duration: 40-60
   gitRepo: https://github.com/OpenAngelArena/oaa
#   qq:

tags: 
    - teams
#   - cooperative
#   - survival
#   - towerDefense
#   - freeForAll

supported_languages:
    - en
    - sc
    - ru
#    - tc

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Open Angel Arena"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Open Angel Arena is a modern reimagining of the classic Angel Arena game mode.

-    **All heroes are viable**, including supports, casters and tanks. In fact, supports are absolutely essential to many strategies.
-   **All items are upgradeable**. This allows for a wide range of possible item 
    builds and for an even wider range of possible strategies. Have you ever 
    wanted to play attack damage Elder Titan using the 100% lifesteal talent or 
    use Luna's ultimate to rain death and stuns on an entire team? In Open Angel 
    Arena these strategies are not only possible but actually viable.
-   **New custom items**, designed by the community and carefully balanced before 
    being added.
-   **Bosses have been redesigned to be an integral part of the game and have 
    been made more interesting**. To upgrade items you have to kill bosses, often 
    as early as 10 minutes into the game and then regularly for the rest of the 
    match. A killed boss respawns as a new, stronger boss with new abilities. 
    Bosses in OAA have abilities that you have to deal with while battling them 
    and aren’t simple DPS tanks.
-   **Duels have been redesigned to include objectives that encourage players to 
    fight and enable objective based gameplay**. There are multiple different duel 
    arenas that you can be placed into and you will always be playing in a duel. 
    If there is a 1v1 in one arena there is also a 4v4 in a different one and so 
    on.
-   **To win the game you must win a Final Duel**. The first team to 100 points
    will trigger a Final Duel. **The team with 100 points then needs to win this 
    Final Duel to win the game**. If they don’t then the games score limit gets 
    extended by 10, putting it into overtime. This goes on until a team wins a 
    Final Duel.
-   Fountain camping has been eliminated from the game.
-   Our custom music is amazing!

Open Angel Arena is an open source project.We are always looking for new people to help with the game. It’s a fun experience and if you are a new developer the best experience you can get is to simply work on a game. If you work on Open Angel Arena you can also get feedback on your work by professional developers. You can find the developer discord in the installation instructions on GitHub.