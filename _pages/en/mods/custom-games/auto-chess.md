---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-auto-chess
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 1613886175
    creator: Drodo Studios
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
    playerCount: 1-8
    duration: 15-45
    qq: 527980259
#   gitRepo:

tags: 
#   - cooperative
#   - survival
#   - teams
#   - towerDefense
    - freeForAll
#   - beta
#   - rpg

supported_languages:
    - en
    - sc
    - tc
    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-gem-td

#---- Localizable Content ----#
title: "DotA Auto Chess"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Hard-working couriers in dota games live in a little island -- Drodo Island. Busy jobs do not prevent them from having fun themselves in their spare time.
- A casino opened on Drodo Island recently, after the TD house and Rink! You can pick DotA heroes as your chess pieces and they will automatically fight for you on a 8*8 chessboard.
- A guide '?' on topleft is for beginners.
- Please do not hesitate to contact if you have any suggestions.
- Please enter the game with a x64 system.