---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-together-we-stand
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
   steamWorkshop: 323196916
   creator: Noob Humiliator
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
   playerCount: 1-5
   duration: 30-45
#   qq:
#   gitRepo:

tags: 
    - cooperative
    - survival
#   - teams
#   - towerDefense
#   - freeForAll
#   - beta
#   - rpg

supported_languages:
     - en
     - sc
#    - tc
#    - ru

brokenByValve: 2019-12-22 # If a valve patch breaks

related_mods:
    - mod-evolve-island

#---- Localizable Content ----#
title: "Together We Stand"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Cooperative survival game.
You don't have to protect anyone except yourself! Just stay alive! 

Every round is a special challenge - strategy and cooperation is the key to win the game. If you are confused about the monsters, just pause the game and read their ability descriptions, it will help you to defeat them. 

You can buy and sell spells in the spell shop, and upgrade them in the spell panel.
Ultimate skills may cost more points to learn.
Heroes with weak model (especially ranged agility heroes) have special powerful ability. 