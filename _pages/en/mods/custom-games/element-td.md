---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-element-td
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 626780182
    creator: Karawasa
    discord: https://discord.gg/2se8VCq
    website: http://www.eletd.com
    playerCount: 1-8
    duration: 20-40
#   qq:
#   gitRepo:

tags: 
#   - cooperative
#   - survival
#   - teams
    - towerDefense
#   - freeForAll
#   - beta
#   - rpg

supported_languages:
    - en
    - sc
    - tc
    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-gem-td
    - mod-spin-td

#---- Localizable Content ----#
title: "Element TD"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Light, Darkness, Water, Fire, Nature, and Earth elements come together in this TD focused on combinations.

Development Team
- Designers: Karawasa, WindStrike, MrChak
- Programmers: Noya, A_Dizzle, Quintinity
- Mapper: Azarak

Translators
- Chinese: Rabbitism - DesTiny
- Thai: ρyro
- Russian: gogolik

Special Thanks
- Naux - For fixing auto-attack post version 7.20
- Mayheim - Custom colored icons
- CrazyRussian - Element icons and workshop image
- BMD - Barebones libraries
- Myll - ModKit
- jimmydorry - GetDotaStats
- Automedic - Elemental orb particles
- Everyone on the IRC over at #dota2mods@GameSurge