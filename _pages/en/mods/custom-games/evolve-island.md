---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-evolve-island
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
  steamWorkshop: 1774569905
  creator: Noob Humiliator
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
  playerCount: 2-12
  duration: 15-30
  qq: 939178127
#   gitRepo:

tags: 
#   - cooperative
#   - survival
    - teams
#   - towerDefense
    - freeForAll
#   - beta
#   - rpg

supported_languages:
    - en
    - sc
#    - tc
#    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-together-we-stand

#---- Localizable Content ----#
title: "Dota Evolve Island"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

A remake of Classic game Darwin's Island in Dota2.
- The Map island_1x10 is for single player. Map island_3x4 is for three player cooperation.
- Survive, eat, and evolve, what you eat determines what you become.
- There are six perks in game: Element, Mystery, Hunt, Durable, Decay, Fury. The perks will
  affect evolved models and abilities.
- Take advantage of Rune, Air-Drop, Power Field to quickly gain experience and high-level items.
- When any player reaches level 11, the game enters Death Mode. Creatures can't respawn, and the last survivor wins the game. 