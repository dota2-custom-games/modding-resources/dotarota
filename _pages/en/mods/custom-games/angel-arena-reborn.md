---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2019-08-15

#---- Language and Localization ----#
content_id: mod-angel-arena-reborn
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 500020226
    creator: Disorance
    playerCount: 10
    duration: 30-45
#   discord: https://discord.gg/cR6r4BV
#   website: http://website.com
#   qq: 564389368
#   gitRepo:

tags: 
    - teams
#   - cooperative
#   - survival
#   - towerDefense
#   - freeForAll

supported_languages:
    - en
    - sc
    - ru
#    - tc

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Angel Arena Reborn"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Angel Arena is back in Dota 2, original bosses, original heroes with a little balance.
Angels and Monks. Demons and Guardians.

There are 12+ heroes from WC3!
All standard heroes are allowed too!
Some heroes has been deleted for balance, and you cant pick then, but you can change your hero to them when you kill hero guardian.

All heroes rebalanced, some heroes too have new skills.

Duel every 5 minutes, 2 minutes to draw.
Custom creeps for 1, 5, 15, 20, 25, 40, 60, 80, 100 levels!
Curently you can level up your hero to 100.

Some change to drop items from creeps.
There are over 100 new items!
Game will ends on several teamkills (you can choose it in game start)