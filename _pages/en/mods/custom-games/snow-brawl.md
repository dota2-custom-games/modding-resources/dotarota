---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2019-08-15

#---- Language and Localization ----#
content_id: mod-snow-brawl
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    creator: Shooper
    playerCount: 2-8
    duration: 10-20
    steamWorkshop: 1200619594
#   website:
#   qq:
#   gitRepo:
#   discord: 

tags: 
    - freeForAll
#   - cooperative
#   - survival
#   - teams
#   - towerDefense

supported_languages:
    - en
    - sc
    - tc

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-swat-reborn

#---- Localizable Content ----
title: "Snow Brawl"

---

Every year, the Tuskan tribes of the North gather for a Frostivus tradition; see who can roll into the largest snowball! But this contest is an anything goes bloodbath with arrows, boulders, and, of course, crushing the other competitors under ones own snowball. This is Snow Brawl.

-    2-8 players. 5+ recommended.
-    Right click to start rolling towards that location.
-    Pick up snow piles to grow larger. First to reach a size of 500 wins.
-    You can crush snowballs that are smaller than you to take their snow.
-    Pick up presents to gain single use abilities.
