---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-amethyst-brawl
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
   steamWorkshop: 1670409309
   creator: Efe
   discord: https://discord.gg/bNR9ezt
#   website: http://website.com
   playerCount: 2-6
   duration: 10-15
#   qq:
#   gitRepo:

tags: 
#   - cooperative
#   - survival
   - teams
#   - towerDefense
#   - freeForAll

supported_languages:
    - en
#    - sc
#    - tc
#    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Amethyst - Dota Brawl"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Action team arena brawl. Use the power of the middle Amethyst to increase your chances of wining.

- Move your hero with WASD Controls.
- Aim and attack with your mouse.
- Almost all abilities are skill shots, including basic attacks.
- Best of 3 format.
- Kill all the enemies to win the round.
- Heroes are re-imaginations of original Dota 2 heroes.
- Heroes generate mana by attacking or landing spells.
- Heroes have 4 basic attacks, 3 specials and 1 ultimate.
- Heroes have a healing limit, so battles mantain a short duration.
- Destroy the middle Amethyst at the center of the map to gain mana and life for you and your team.

Good luck and have fun!