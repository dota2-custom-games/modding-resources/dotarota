---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2019-08-15

#---- Language and Localization ----#
content_id: mod-overthrow-2
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
   steamWorkshop: 1517661692
   creator: Dota 2 Unofficial
   playerCount: 10
   duration: 20
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
#   qq:
#   gitRepo:

tags: 
    - freeForAll
    - teams
#   - cooperative
#   - survival
#   - towerDefense

supported_languages:
    - en
    - sc
    - ru
#    - tc

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "Overthrow 2.0"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

Slay your enemies! Be the first to earn the goal number of kills, or the team with the most kills when the round ends, and you will be victorious.

## How to play
- Approach the Midas Throne to earn bonus gold and experience.
- Snatch up any giant coins the Over Boss tosses to earn even more gold.
- Keep an eye out for item deliveries. An icon will appear on your map where an item will appear.
- The fewer kills your team has, the better the item you will receive from a delivery.
- Slay anyone who isn't on your team.
