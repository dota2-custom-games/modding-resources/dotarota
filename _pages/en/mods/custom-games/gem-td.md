---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-gem-td
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    steamWorkshop: 474619917
    creator: Drodo Studios
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
    playerCount: 1-4
    duration: 30-60
    qq: blue-lobster
#   gitRepo:

tags: 
    - cooperative
#   - survival
#   - teams
    - towerDefense
#   - freeForAll
#   - beta
#   - rpg

supported_languages:
    - en
    - sc
    - tc
#   - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-auto-chess
    - mod-element-td
    - mod-spin-td

#---- Localizable Content ----#
title: "GemTD"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

GemTD is a tower defense map that originated from Warcraft 3.
It features a unique maze, a challenging difficulty curve and a high degree of replayability.