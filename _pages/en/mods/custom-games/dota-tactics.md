---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2020-01-13

#---- Language and Localization ----#
content_id: mod-dota-tactics
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
   steamWorkshop: 1959343503
   creator: Wisakedjak
   discord: http://discord.gg/Zds7CwN
   playerCount: 2-8
#   website: http://website.com
   duration: 30-45
#   qq:
#   gitRepo:

tags: 
#   - cooperative
#   - survival
    - teams
    - beta
#   - towerDefense
#   - freeForAll

supported_languages:
    - en
#    - sc
#    - tc
#    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "DotA Tactics (Beta)"

# ----------------------------#
# /images/custom-game/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/custom-game/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/custom-game/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---

DotA Tactics is a DotA mod that combines the core essence of DotA, Xcom and Autochess into a spectacular tactics game. Two teams of 4 players each are pinned against each other, each player controlling a single hero. Players take turns simultaneously and must use each hero's unique set of abilities to attack enemy players or avoid incoming attacks.
