---

#---- Page Properties ----#
layout: mod
contentType: custom-game
post-date: 2019-08-11

#---- Language and Localization ----#
content_id: mod-swat-reborn
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
    creator: Shooper
    discord: http://discord.gg/coolKidsOnly
    website: http://swatreborn.com
    playerCount: 1-9
    duration: 15-90
    steamWorkshop: 725282388
#   qq:
#   gitRepo:

tags: 
    - cooperative
    - survival
#   - teams
#   - towerDefense
#   - freeForAll

supported_languages:
    - en
    - sc
    - tc

# brokenByValve: 2019-08-12 # If a valve patch breaks

related_mods:
    - mod-snow-brawl

#---- Localizable Content ----#
title: "SWAT: Reborn"

---

Cooperative map with a goal of completing objectives while surviving waves of computer-controlled enemies. Teamwork is essential as is using each class's strengths to their fullest. Enemies are numerous and extremely dangerous, resulting in non-stop and intense action.

- 1-9 players. 5+ recommended.
- 8 unique classes with 8-12 custom abilities with extra customization through armor, traits, and specializations.
- Your progress is tracked and saved through repeated games - rank brings prestige and minor perks, but never outweighs player skill.
- Four objectives-based difficulty levels that scale every aspect of the game, providing a fun challenge to new players and pros alike. Each difficulty is significantly harder than the previous, with higher difficulties adding additional challenges to overcome. 
- Extensive randomization ensures a new experience every time.