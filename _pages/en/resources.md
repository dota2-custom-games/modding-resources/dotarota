---
# Page Properties
layout: default
navRanking: 4

# Language and Localization
content_id: resources
lang: en

# Localizable Content
title: Resources
---

## Resources

These pages contain resources to help in the creation of DotA 2 Arcade mods.

- [Libaries](libraries/) - A list of libraries which can be used to add new functionality to your DotA mod
- [Tutorials](tutorials/) - Tutorials to help you get started in the modding world

## Useful Links

The following (external) resources are useful:

- [Panorama Source Files](http://dotabase.dillerm.io/dota-vpk/?dir=panorama) - Panorama taken from the DotA source files, useful for copying core DotA layouts or styles