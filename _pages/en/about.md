---

# Page Properties
layout: default
hide_from_nav: 1

# Language and Localization
content_id: about
lang: en

# Localizable Content
title: About

---

## Contributing

DotaRota is generated from a curated list of Dota 2 arcade mods and resources for their creation. If you'd like to have your mod or resource listed on this
site, please send an email to admin@dotarota.com, or get in touch on the Discord server. We will need:

- The name of the mod and workshop link
- A brief description of the mod
- Some ingame screenshots
- Any additional information that should be included on the page

## Sources

The following resources were used in building this website:

- [icons8.com](https://www.icons8.com) provide some icons used around the site.
- [flag-icon-css](https://github.com/lipis/flag-icon-css) by lipis provides flag icons for localisations.
- [Lightbox](https://github.com/sachinchoolur/lightgallery.js), by sachinchoolur, is used to generate the albums used for mods and libraries.

The site is made using [Jekyll](https://jekyllrb.com/), a static site generator.

## Special Thanks

The DotA modding community, and thus this website, would not exist without the extensive contributions made by several people. We give special thanks to Noya and BMD.

## Legal

DotaMods is a curated list of resources and does not own any of the referenced materials. Dota is a copyright of Valve Corporation. For any concerns please email us at admin@dotarota.com