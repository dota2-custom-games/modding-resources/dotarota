---

# Page Properties
layout: plainList
listType: tutorial
includeCounter: true

# Language and Localization
content_id: tutorials
lang: ru

# Localizable Content
title: Tutorials
---

## Tutorials

The following guides should help in the creation of DotA mods. They are sorted approximately in order of complexity.