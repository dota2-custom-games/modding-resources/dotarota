/* -------------------------------------------------------- */
/* ---------------------- Util Stuff ---------------------- */
/* -------------------------------------------------------- */

function localize(string, locale){

    if (localizations[string]) {
        return localizations[string][locale]
    } else {
        return string
    }
}

function nthIndex(str, pat, n){
    var L= str.length, i= -1;
    while(n-- && i++<L){
        i= str.indexOf(pat, i);
        if (i < 0) break;
    }
    return i;
}

/* -------------------------------------------------------- */
/* ---------------------- PRNG Stuff ---------------------- */
/* -------------------------------------------------------- */
// Thanks stackoverflow!

var m_w = 123456789;
var m_z = 987654321;
var mask = 0xffffffff;

function seed(i) {
    m_w = (123456789 + i) & mask;
    m_z = (987654321 - i) & mask;
}


function random()
{
    m_z = (36969 * (m_z & 65535) + (m_z >> 16)) & mask;
    m_w = (18000 * (m_w & 65535) + (m_w >> 16)) & mask;
    var result = ((m_z << 16) + (m_w & 65535)) >>> 0;
    result /= 4294967296;
    return result;
}

// I wrote this one myself, like a grownup
function randomFromList(list)
{
    var rand = random();
    var index = Math.floor(rand*list.length);
    return list[index];
}

/* -------------------------------------------------------- */
/* ------------------ Main Load Function ------------------ */
/* -------------------------------------------------------- */

function selectModsOfTheDay(modList, dotaVariantList) {
    console.log("Initializing list!");

    var todaysMod = randomFromList(modList);
    // Check for a localised version
    todaysMod = todaysMod || (
        modList.filter(mod =>
            mod[1] == locale && // lang
            mod[3] == todaysMod[3] // content_id
        )
    );
    loadModIntoContainer(todaysMod,
         document.getElementById("todaysModContainer"));

    
    var todaysDotaVariant = randomFromList(dotaVariantList);
    // Check for a localised version
    todaysDotaVariant = todaysDotaVariant || (
        modList.filter(mod =>
            mod[1] == locale && // lang
            mod[3] == todaysDotaVariant[3] // content_id
        )
    );
    loadModIntoContainer(todaysDotaVariant,
         document.getElementById("todaysDotaVariantContainer"));
    
}

function loadModIntoContainer(mod, container) {
    var title = mod[0];
    var lang = mod[1];
    var contentType = mod[2];
    var content_id = mod[3];
    var infoBoxContent = mod[4];
    var description = mod[5];
    var url = mod[6];
    var tags = mod[7];
    

    // Link
    container.href = url

    // Image
    var imagePanel = container.querySelector(".cardImage");

    imagePanel.data = baseUrl+
            "/images/"+
            contentType+
            "/"+
            content_id+
            "/card.png";

    // Backup SVG
    var svgPanel = container.querySelector(".svgText");
    svgPanel.textContent = title;

    // Title
    var titlePanel = container.querySelector(".descriptionTitle");
    titlePanel.textContent = title;

    // Time
    var timePanel = container.querySelector(".gameTime");
    timePanel.textContent = infoBoxContent.duration + timePanel.textContent;

    // Playercount
    var playerCountPanel = container.querySelector(".playerCount");
    playerCountPanel.textContent = infoBoxContent.playerCount;

    // Tags
    var tagsPanel = container.querySelector(".tags");
    var tags = tags;
    tags = tags.map(x => localize(x, locale));

    tagsPanel.textContent = tags.join(", ");

    // Description
    
    var descriptionPanel = container.querySelector(".descriptionText");
    var description = description;
    var thirdPeriod = nthIndex(description, ".", 3);
    if (thirdPeriod > 0) {
        description = description.slice(0, thirdPeriod+1);
    }

    descriptionPanel.innerHTML = description;
    

}


/* -------------------------------------------------------- */
/* -------------------------- IIFE ------------------------ */
/* -------------------------------------------------------- */


window.onload = function() {

    var MS_IN_DAY = 86400000;
    seed(Math.floor(Date.now()/MS_IN_DAY)+2);
    // Rolls for luck
    random();
    random();

    selectModsOfTheDay(modList, dotaVariantList)
}