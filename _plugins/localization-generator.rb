module Jekyll

    # -------------------------------------------------------#
    # ------------------ Localization Generator -------------#
    # -------------------------------------------------------#

    # Localization Generator produces a chinese, russian etc. stub
    # for every /en/ page that doesn't have a proper translation
    class LocalizationGenerator < Generator
        safe true

        def generate(site)
            puts '-----------------------------------'
            puts 'Localization Generator is running!'

            # loop over all pages with lang: en
            enPages = site.pages.select do |page|
                page.data['lang'] == 'en' and
                !page.data['content_id'].nil?
            end

            enPages.each do |enPage| 
                #puts 'Making localisation stubs for page: '+enPage.data['content_id']

                # loop over all other languages
                ['cn', 'ru'].each do |language|
                    # look to see if a page exists with the same id
                    localizedPages = site.pages.select do |page|
                        #puts 'Checking page: '+page.dir+page.name
                        page.data['lang'] == language and
                        page.data['content_id'] == enPage.data['content_id']
                    end
                    if localizedPages.empty? 
                        # if not, create a new stub
                        
                        #puts 'Creating a stub for page: '+enPage.data['content_id']+', name: '+enPage.name+' and language: '+language
                        stub = enPage.name[0..-4]
                        if stub.nil? or stub == "index"
                            stub = ""
                        end
                        middleDir = enPage.dir[11..-1]
                        if middleDir.nil? or middleDir.empty?
                            middleDir = ''
                        else
                            middleDir = middleDir+'/'
                        end
                        newDir = '/'+language + '/' + middleDir + stub
                        site.pages << LanguageStub.new(site, site.source, newDir, 'index.html', enPage, language)
                    end
                end
            end
            puts '-----------------------------------'
        end
    end

    # Class for the page created by languageGenerator
    class LanguageStub < Page 
        #def initialize(site, originalPage, language)
        def initialize(site, base, dir, name, originalPage, lang)

            @site = site
            @base = base
            @dir = dir 
            @name = name

            self.process(@name)
            self.read_yaml(File.join(base, '_layouts'), originalPage.data['layout']+'.html')

            self.data['lang'] = lang
            self.data['content_id'] = originalPage.data['content_id']
            self.data['title'] = originalPage.data['title']
            if !originalPage.data['listType'].nil?
                self.data['listType'] = originalPage.data['listType']
            end
            if !originalPage.data['contentType'].nil?
                self.data['contentType'] = originalPage.data['contentType']
            end
            self.data['isStub'] = true

            if !originalPage.data['infoBoxContent'].nil?
                self.data['infoBoxContent'] = !originalPage.data['infoBoxContent']
            end

            #puts 'I added a new page in folder: '+self.dir

        end

    end
end