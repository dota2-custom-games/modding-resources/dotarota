import sys
from PIL import Image
import os

imagePath = sys.argv[1]

def isImageFile(file):
    if not os.path.isfile(file):
        return False
    if (file.endswith(".png") or
        file.endswith(".jpg")
    ):
        return True

    return False

if isImageFile(imagePath):
    #print("Resizing image at path: "+imagePath)
    img = Image.open(imagePath)
    madeChange = False
    while (img.size[1] > 400 or img.size[0] > 300):
        madeChange = True
        img = img.resize( (int(img.size[0]/2), int(img.size[1]/2)), Image.ANTIALIAS)
    if madeChange:
        img.save(imagePath)