require 'jekyll'
require_relative './premonition/version'
require_relative './premonition/resources'
require_relative './premonition/hook'

#
# The majority of this source code is directly copied from the premonition extension
# https://github.com/lazee/premonition
# 
# The only change is to the regex in the hook, to weaken the search for newlines
# This was causing a problem on some machines due to windows newlines shenanigans
#

module Jekyll
  module Premonition
    puts "Initializing Premmy!"
  end
end