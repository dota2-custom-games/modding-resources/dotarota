module Jekyll


    # -------------------------------------------------------#
    # ------------------ Redirect Generator -----------------#
    # -------------------------------------------------------#

    # Redirect Generator produces a site in / for every site in /en/
    # This is so e.g. dotarota.com is still a website (the /en/ is added later)
    class RedirectGenerator < Generator
        safe true

        def generate(site)

            puts '-----------------------------------'
            puts 'RedirectGenerator Generator is running!'
            enPages = site.pages.select do |page|
                page.data['lang'] == 'en' and
                !page.data['content_id'].nil?
            end

            enPages.each do |enPage|
                #puts 'nextpage: '+enPage.name
                stub = enPage.name[0..-4]
                if stub =='index'
                    stub = ''
                end
                middleDir = enPage.dir[11..-1]
                if middleDir.nil? or middleDir.empty?
                    middleDir = ''
                end
                newDir = '/' + middleDir + stub
                #puts 'New directory: '+newDir
                site.pages << RedirectStub.new(site, site.source, newDir, 'index.html', enPage)
            end

            puts '-----------------------------------'

        end
    end

    # Class for the page created by redirectGenerator
    class RedirectStub < Page
        def initialize(site, base, dir, name, originalPage)

            #puts 'dir?: '+dir
            #puts 'base?: '+base
            #puts 'name?: '+name

            @site = site
            @base = base
            @dir = dir 
            @name = name

            self.process(@name)
            self.read_yaml(File.join(base, '_layouts'), originalPage.data['layout']+'.html')

            #puts originalPage.data['content_id']

            self.data['title'] = originalPage.data['title']
            self.data['content_id'] = originalPage.data['content_id']
            self.data['duplicateFromOriginal'] = true

            #puts 'I added a new page in folder: '+self.dir

            #puts 'finished!'
        end

    end
end