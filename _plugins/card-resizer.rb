module Jekyll


    # -------------------------------------------------------#
    # --------------------- Card Resizer --------------------#
    # -------------------------------------------------------#

    # This creates a cute "smaller" card for each full-sized card image file
    # If we're loading 10-20+ cards on a page, then loading all of them can be costly
    # We're going to use a python script to do it, because Ruby ImageMagick is just... weird

    class CardResizer < Generator
        safe true

        def generate(site)

            puts '----------------------------------------'
            puts 'CardResizer is running!'
            cardImages = site.static_files.select do |file|
                file.basename == 'card'
            end

            cardImages.each do |cardImage|
                path = cardImage.path
                result = `python3 ./_plugins/card-resizer/resizeCard.py #{path}`
                #puts result
            end
        end
    end

end