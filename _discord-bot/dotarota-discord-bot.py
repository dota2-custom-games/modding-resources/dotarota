# My raspberry pi only supports python 3.5, so hacky ^

import asyncio
import time
import sys
import os

import numpy as np
import datetime
import time
import math

import discord
from discord.ext import commands

from dotenv import load_dotenv

import yaml

# Pip3 libraries:
# discord.py asyncio pyyaml numpy python-dotenv future_fstrings

# Removes all warnings!
# Numpy throws these from PRNG class because of integer overflow
# Find a better way of squelching just these?
import warnings
warnings.filterwarnings("ignore")

# Looks like a space, but counts as a character
# So discord will not squelch it as if it were whitespace
ZERO_WIDTH_SPACE = '\u200B'
REVERSE_LINE_FEED = '\u008D'
SEC_IN_DAY = 86400

CUSTOM_GAMES_CHANNEL = "custom-games-search"
DOTA_VARIANTS_CHANNEL = "dota-variants-search"
DEBUG_CHANNEL = "bot-testing"

localisations = {}

#client = discord.Client()
client = commands.Bot(command_prefix="!")

#-----------------------------------------------------------------------------#
#-------------------------------- Bot Commands -------------------------------#
#-----------------------------------------------------------------------------#

@client.command(name='gotd')
async def gotd_command(context):
    cmdChannel = context.message.channel
    if (cmdChannel.name == CUSTOM_GAMES_CHANNEL):
        output = getArcadeModEmbed(CGOfTheDay, custom_game_list)
    if (cmdChannel.name == DOTA_VARIANTS_CHANNEL):
        output = getArcadeModEmbed(DVOfTheDay, dota_variant_list)

    if (output):
        await cmdChannel.send(embed=output)


#-----------------------------------------------------------------------------#
#----------------------------- PRNG Definitions ------------------------------#
#-----------------------------------------------------------------------------#
# We want to get the same "random" choices as the website does every day
# So we use the same PRNG that the website does.
# Except the website is written in javascript
# So we have to use type enforcement from numpy so that >> works the same way
# Don't ask me where all these numbers come from
# Also don't ask me why we don't just use a hash instead of a PRNG
# When we're only making two rolls anyway.

def zero_fill_shift(val, n):
    return (val >> n) if val >= 0 else ((val + 0x100000000) >> n)

class PRNG:
    def __init__(self, seed):
        self.mask = 0xFFFFFFFF
        self.m_w = np.int32(123456789 + seed) & self.mask
        self.m_z = np.int32(987654321 - seed) & self.mask

    def random(self):
        mini = 0xFFFF
        self.m_w = np.int32((18000 * (self.m_w & mini) + (self.m_w >> 16)) & self.mask)
        self.m_z = np.int32((36969 * (self.m_z & mini) + (self.m_z >> 16)) & self.mask)
        result = ((self.m_z << 16) + (self.m_w & mini)) 
        result = zero_fill_shift(result, 0)
        result = result / 4294967296
        return result

    # This function has side effects!
    def randomFromList(self, list_in):
        rand = self.random()
        index = math.floor(rand*len((list_in)))
        return list_in[index]

#-----------------------------------------------------------------------------#
#-------------------------- Called/Scheduled Tasks ---------------------------#
#-----------------------------------------------------------------------------#

# TODO: Abstract to support localization
# TODO: Abstract to include other information (e.g. tags)
def getArcadeModEmbed(gameID, mod_list):
    gameInfo = mod_list[gameID]

    # Premake some strings
    link = gameInfo['link']
    linkString = f'[Steam Workshop]({link})'
    tagsString = "🏷️: " + (", ").join([localise(tag, "en") for tag in gameInfo['tags']])
    timeString = '🕖: '+str(gameInfo['duration'])+localise("duration-minutes", "en")
    playerString = '👥: '+str(gameInfo['playerCount'])

    # Create Embed
    embed = discord.Embed(
        description = gameInfo['description']
        , colour = discord.Colour.blue()
    )

    embed.set_image(url=gameInfo['cardImagePath'])
    embed.set_author(name=gameInfo['title'], url=link)

    embed.add_field(name="Link",
        value=linkString,
        inline=False
    )
    embed.add_field(name="Author",
        value=gameInfo['creator'],
        inline=False
    )

    embed.add_field(name="Properties", # TODO: Good name???
        value= f'{timeString}\n{tagsString}',
        inline=True
    )
    embed.add_field(name=ZERO_WIDTH_SPACE,
        value=playerString,
        inline=True
    )

    embed.set_footer(text="#GameOfTheDay")
    return embed

# Scheduled task that runs every 60 seconds.
# Selects and then chooses the custom game + dota variant
async def select_daily_game_task():
    await client.wait_until_ready()
    await asyncio.sleep(5) # Dirty hack to make sure on_ready has actually finished running
    select_daily_game() # Set it once, just to make sure that e.g. !gotd can activate it after a reset

    # Find out how long until the next minute (e.g.) starts
    timeNow = datetime.datetime.now()
    delta = datetime.timedelta(minutes=1)
    nextMinute = (timeNow + delta).replace(second=0)
    timeToWait = (nextMinute-timeNow).seconds

    await asyncio.sleep(timeToWait)
    while True:
        select_daily_game()
        await print_daily_game()

        # Calculate next wait time
        timeNow = datetime.datetime.now()
        delta = datetime.timedelta(minutes=1)
        nextMinute = (timeNow + delta).replace(second=0)
        timeToWait = (nextMinute-timeNow).seconds

        # Actually wait
        await asyncio.sleep(timeToWait)

CGOfTheDay = False
DVOfTheDay = False
def select_daily_game():
    global CGOfTheDay
    global DVOfTheDay

    epochDays = math.floor(time.time()/SEC_IN_DAY)
    print("    Setting daily games for day number: "+str(epochDays))
    dailyprng = PRNG(epochDays+2) # 2 just for luck!
    dailyprng.random() # Extra rolls just for luck!
    dailyprng.random()

    CGOfTheDay = dailyprng.randomFromList(sorted(list(custom_game_list.keys())))
    DVOfTheDay = dailyprng.randomFromList(sorted(list(dota_variant_list.keys())))

async def print_daily_game():
    cgEmbed = getArcadeModEmbed(CGOfTheDay, custom_game_list)
    dvEmbed = getArcadeModEmbed(DVOfTheDay, dota_variant_list)

    await client.debugChannel.send(embed=cgEmbed)
    await client.debugChannel.send(embed=dvEmbed)

#-----------------------------------------------------------------------------#
#------------------------------ Localization ---------------------------------#
#-----------------------------------------------------------------------------#

PATH_TO_LOCALISATIONS = "../_data/localisations.yml"
def load_localisations():
    with open(PATH_TO_LOCALISATIONS, 'r', encoding='utf8') as stream:
        yml = yaml.safe_load(stream)
    return yml

def localise(string, lang):
    return localisations[string][lang]

#-----------------------------------------------------------------------------#
#---------------------------------- Setup ------------------------------------#
#-----------------------------------------------------------------------------#

STEAM_WORKSHOP_BASEURL = "https://steamcommunity.com/sharedfiles/filedetails/?id="
PATH_TO_IMAGES = "https://dotarota.com/images/"
DESCRIPTION_LINES_INCLUDED = 3
# Returns a dict of id-->information
def load_mods_from_folder(folder):
    output = {}
    for filename in os.listdir(folder):
        if filename.endswith(".md"):
            # Open the file and strip the information
            with open(folder+"/"+filename, "r", encoding='utf8') as yml:
                #docs = yaml.load_all(yml, Loader=yaml.SafeLoader)
                # Retrieve the YAML Part:
                information = ""
                # First line is always "---" for some reason
                yml.readline() # So skip line 1
                for line in yml:
                    if (line.startswith("---")):
                        break # Done reading the yaml part
                    else:
                        information = information+line
                information = yaml.load(information, Loader=yaml.SafeLoader)

                print("    Adding Entry for game: "+information["title"])

                # The description is the rest of the file after the "---"
                # We'll read 3 lines from it.
                # That sounds like a nice number of lines to read
                description = ""
                for i in range(DESCRIPTION_LINES_INCLUDED):
                    description += yml.readline()

                # Commit a new entry to the dict
                # I'd rather do this manually than copy it across directly
                # But I don't know why yet.
                infoBoxContent = information["infoBoxContent"]
                newEntry = {}
                newEntry["title"] = information["title"]
                newEntry["tags"] = information["tags"]
                newEntry["duration"] = infoBoxContent["duration"]
                newEntry["creator"] = infoBoxContent["creator"]
                newEntry["playerCount"] = infoBoxContent["playerCount"]
                newEntry["link"] = STEAM_WORKSHOP_BASEURL+str(infoBoxContent["steamWorkshop"])
                newEntry["description"] = description
                newEntry["cardImagePath"] = PATH_TO_IMAGES+information["contentType"]+"/"+information["content_id"]+"/card.png"
                output[information["content_id"]] = newEntry

    return output

# Called after the bot first connects to the server
@client.event
async def on_ready():
    for g in client.guilds:
        if g.name == GUILD:
            client.guild = g
    for channel in client.guild.channels:
        if channel.name == "bot-testing":
            client.debugChannel = channel

    await client.debugChannel.send("Live!")

#-----------------------------------------------------------------------------#
#---------------------------- Initialization ---------------------------------#
#-----------------------------------------------------------------------------#

# Get hidden environment variables (safety first, kids!)
print("---- DotaRota Discord Bot Initialise ----")
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
if TOKEN is None:
    sys.exit("---- Tokens failed to load, do you have the .env file? ----")


# Loads the various game information into dictionaries
PATH_TO_CUSTOM_GAMES = "../_pages/en/mods/custom-games/"
PATH_TO_DOTA_VARIANTS = "../_pages/en/mods/dota-variants/"
print("---- Loading Custom Games Lists ----")
custom_game_list = load_mods_from_folder(PATH_TO_CUSTOM_GAMES)
dota_variant_list = load_mods_from_folder(PATH_TO_DOTA_VARIANTS)

# Get Localization strings from the website files
localisations = load_localisations()

# PRNG that is to be used for commands (e.g. for random games)
commandPRNG = PRNG(time.time())

# This adds an asynchronous task to spam the messages of the day
# I still don't actually understand how it works, but okay.
client.loop.create_task(select_daily_game_task())

# Actually kick off the bot
print("---- Firing up the Bot ----")
client.run(TOKEN)