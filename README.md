# DotaRota

This repository contains the material used for the DotaRota website, currently
hosted at dotarota.com. The website serves as a community portal for the Dota 2
Arcade, highlighting custom games and resources for creating them.

The site runs off Jekyll. More information about the resources and plugins used
can be found in the about pages on the site.

This repository also contains code for a discord bot, written in Python. The bot
synchronises data with the website, and runs on a discord server to advertise
these games and allow filtering and selecting random games by tag and other
properties.