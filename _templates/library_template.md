---

#---- Page Properties ----#
layout: library
contentType: library
post-date: YYYY-MM-DD

#---- Language and Localization ----#
content_id: library-LIB-NAME
lang: en

#---- Library-only Stuff ----#
infoBoxContent:
#   creator: Steve-o
#   gitRepo:

tags: 
#   - UI
#   - util
#   - reference

#dependencies:
#    - library-timers

#related_libraries:
#   - library-timers

#---- Localizable Content ----#
title: "LIBRARY NAME"

# ----------------------------#
# /images/library/library-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/library/library-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this library's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this library (in the corresponding language)
# ----------------------------#

---
