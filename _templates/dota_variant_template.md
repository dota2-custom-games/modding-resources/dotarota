---

#---- Page Properties ----#
layout: mod
contentType: dota-variant
post-date: YYYY-MM-DD

#---- Language and Localization ----#
content_id: mod-MOD-NAME
lang: en

#---- Mod-only Stuff ----#
infoBoxContent:
#   steamWorkshop: 12341234
#   creator: 
#   discord: http://discord.gg/coolKidsOnly
#   website: http://website.com
#   playerCount: 1-9
#   duration: 15-90
#   qq:
#   gitRepo:

tags: 
#   - cooperative
#   - survival
#   - teams
#   - towerDefense
#   - freeForAll

supported_languages:
#    - en
#    - sc
#    - tc
#    - ru

# brokenByValve: 2019-08-12 # If a valve patch breaks

#related_mods:
#   - mod-snow-brawl

#---- Localizable Content ----#
title: "MOD NAME"

# ----------------------------#
# /images/dota-variant/mod-MOD-NAME/card.png
# Will be shown on the "cards" on the filter pages
# /images/dota-variant/mod-MOD-NAME/feature.png
# Will be shown on this mod's individual page
# /images/dota-variant/mod-MOD-NAME/gallery/---.png
# Will be shown in the "gallery" section on this mod's page

# All images should be 16x9 aspect ratio!

# Below the ---, place the description of this mod (in the corresponding language)
# ----------------------------#

---
