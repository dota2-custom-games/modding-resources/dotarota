---

#--- Page Properties ---#
layout: tutorial
contentType: tutorial
list-ranking: 1
# (Higher list rankings will go LOWER on the tutorial page)

#--- Language and Localization ---#
content_id: tutorial-TUT-TITLE
lang: en

#--- Sections/Headings (Tutorials Only) ---#
sections:
    - "SECTION TITLE"
    - "OTHER SECTION TITLE"

#--- Localizable Content ---#
title: TUTORIAL_TITLE

# ----------------------------#
# The body of the tutorial goes below the ---

# For titles:
#    ## Section title
# And add the title and "slugified" version to sections above
# For galleries:

#   ***
#   <div class="lightgallery" markdown="1">
#   [![Tutorial Image]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png)
#   ]
#        (../../../../images/tutorial/tutorial-TUT-TITLE/IMG.png"/>)
#   </div>
#   ... (repeat images as necessary)
#   ***

# For infoboxes:

# > error "title"
# > Test boop

# Valid infobox "types" are 'note' 'info' 'warning' 'error'
# 'info' should be used for things that are relevant to the tutorial in question
# 'note' should be used for asides (e.g. not directly relevant)
# the other two should remain unused unless it's important

#   Then convert the link to a single line
# ----------------------------#

---
