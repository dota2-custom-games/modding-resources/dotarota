---

#--- Page Properties ---#
layout: default
contentType: blog
hide_from_nav: 1

#--- Language and Localization ---#
content_id: blog-YYYY-MM-DD
lang: en


#--- Blog Specific Things ---#
post-date: YYYY/MM/DD

#--- Localizable Content ---#
title: BLOG_POST_TITLE

# ----------------------------#
# The body of the blog post goes below the ---
# Use standard markdown
# Should probably add new features to blog posts at some point?
# ----------------------------#

---
